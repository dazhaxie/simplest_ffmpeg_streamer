# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/gitee-workspace/simplest_ffmpeg_streamer/simplest_ffmpeg_streamer/simplest_ffmpeg_streamer.cpp" "D:/gitee-workspace/simplest_ffmpeg_streamer/simplest_ffmpeg_streamer/cmake-build-debug/CMakeFiles/simplest_ffmpeg_streamer.dir/simplest_ffmpeg_streamer.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "MSVC")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../include/libavcodec"
  "../include/libavdevice"
  "../include/libavfilter"
  "../include/libavformat"
  "../include/libavutil"
  "../include/libpostproc"
  "../include/libswresample"
  "../include/libswscale"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
